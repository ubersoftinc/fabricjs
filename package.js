Package.describe({
  name: 'ubersoft:fabricjs',
  version: '1.7.9_12',
  summary: 'Wrapper for Fabric.js, a javascript library for working with canvas, changed IText class',
  git: 'https://bitbucket.org/ubersoftinc/fabricjs.git',
  documentation: 'README.md'
});

Package.onUse(function(api) {
  api.versionsFrom('1.4.3.2');
  api.use(['meteor', 'ddp', 'jquery']);
  api.addFiles('client/fabric.min.js', ['client']);
});

Package.onTest(function(api) {
  api.use('tinytest');
  api.use('ubersoft:fabricjs');
  api.addFiles('tests/client/index.js', ['client']);
});
