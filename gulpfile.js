var gulp = require('gulp');
var minify = require('gulp-minify');
var replace = require('gulp-replace');

var jsMinifyLocation = ['./client/fabric.js'];
gulp.task('minify-js', function() {
  gulp.src(jsMinifyLocation)
    .pipe(minify({
      ext:{
        src:'.js',
        min:'.min.js'
      },
      ignoreFiles: ['.min.js']
    }))
    .pipe(gulp.dest('./client'))
});

gulp.task('replace-include', function() {
  gulp.src(['./package.js'])
    .pipe(replace('client/fabric.js', 'client/fabric.min.js'))
    .pipe(gulp.dest('./'));
});

gulp.task('default', [ 'minify-js', 'replace-include' ]);
